FROM ubuntu:16.04
MAINTAINER Colin Powell "colin.powell@gmail.com"
RUN apt -qq update
RUN apt install make
ADD . /opt/apps/pyvan
RUN (cd /opt/apps/pyvan && /usr/bin/make deps_ubuntu_16)
RUN pip3 install virtualenv
RUN (cd /opt/apps/pyvan && make install)
EXPOSE 30321
CMD ["/opt/ve/pyvan/bin/gunicorn", "pyvan.wsgi", "-w 2", "-b 0.0.00:30321"]]
