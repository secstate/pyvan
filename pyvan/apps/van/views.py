from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Project, Library, LibraryVersion


class ProjectListView(ListView):
    model = Project

    def get_queryset(self):
        return Project.objects.all()


class ProjectDetailView(DetailView):
    model = Project

    def get_queryset(self):
        return Project.objects.all()


class ProjectCreateView(CreateView):
    model = Project


class LibraryListView(ListView):
    model = Library


class LibraryDetailView(DetailView):
    model = Library


