# -*- coding: utf-8 *-* 
import sys
import mock
import setuptools
import requirements
from requirements.requirement import Requirement
from django.core.management.base import BaseCommand, CommandError

from van.models import Library, LibraryVersion, Project, ProjectLibraryVersion


class Command(BaseCommand):
    '''
    A management command to analyze a project's dependency versions and add
    libraries when they don't exist.
    '''
    help = "Analyze a project's dependency versions."

    def add_arguments(self, parser):
        parser.add_argument('project_slug', nargs='+', type=str)

    def handle(self, *args, **options):
        for project_slug in options['project_slug']:
            try:
                poll = Project.objects.get(pk=project_slug)
            except Project.DoesNotExist:
                raise CommandError('Project "%s" does not exist' % project_slug)

            req_list = []
            # First grab requirements from setup.py
            with mock.patch.object(setuptools, 'setup') as mock_setup:
                import setup

            args, kwargs = mock_setup.call_args

            install_requires = kwargs['install_requires']

            for req in install_requires:
               req_list += [Requirement.parse_line(req)]

            # Then check for a requirements.txt file
            req_file = open('requirements.txt', 'r')
            req_list += [r for r in requirements.parse(req_file)]

    	    for req in req_list:
                library, created = Library.objects.get_or_create(slug=slugify(req.name))
                if created:
                    library.name = req.name
                    library.save()
                for spec in req.specs:
                    version, created = LibraryVersion.objects.get_or_create(
                        version=spec[1]
                        library=library)

                    pver, created = ProjectLibraryVersion.objects.get_or_create(
                        project=project,
                        version=version,
                    if created:
                        pver.state=spec[0]
                        pver.save()

                return version


                # Find or create the project object
                # Iterate through the libraries finding-or-creating
                # Find or create each library version

	return True
