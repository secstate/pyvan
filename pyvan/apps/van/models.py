from django.db import models
from django.conf import settings


class Project(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    description = models.TextField(blank=True, null=True)
    repo_url = models.URLField(blank=True, null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)


    def __str__(self):
        return "{0}".format(self.name)

class Library(models.Model):
    name = models.CharField(max_length=255)
    pypi_url = models.URLField(blank=True, null=True)
    repo_url = models.URLField(blank=True, null=True)
    slug = models.SlugField()


    def __str__(self):
        return "{0}".format(self.name)


class LibraryVersion(models.Model):
    library = models.ForeignKey(Library)
    version = models.CharField(max_length=100)


    @property
    def major(self):
        return 'Should return the major version as integer'

    @property
    def minor(self):
        return 'Should return minor version as integer'

    @property
    def bugfix(self):
        return 'Should return bugfix version'

    def __str__(self):
        return "{0} {1}".format(self.library, self.version)


class ProjectLibraryVersion(models.Model):
    project = models.ForeignKey(Project)
    version = models.ForeignKey(LibraryVersion)
    state = models.CharField(max_length=3, blank=True, null=True)


    def __str__(self):
        return "{0} {1}".format(self.project, self.version)
