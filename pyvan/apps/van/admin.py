from django.contrib import admin

from .models import Project, Library, LibraryVersion

admin.site.register(Project)
admin.site.register(Library)
admin.site.register(LibraryVersion)
