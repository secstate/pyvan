from django.conf.urls import url, include

from .views import ProjectListView, LibraryListView, ProjectDetailView, LibraryDetailView

urlpatterns = [
    url(r'^libraries/$', LibraryListView.as_view(), name='library-list'),
    url(r'^libraries/(?P<slug>[-\w]+)/$', LibraryDetailView.as_view(), name='library-detail'),
    url(r'^projects/$', ProjectListView.as_view(), name='project-list'),
    url(r'^projects/(?P<slug>[-\w]+)/$', ProjectDetailView.as_view(), name='project-detail'),
]
